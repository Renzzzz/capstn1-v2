﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    Inspectable inspectable;
    LockedItem lockedItem;
    Animator animator;
    AudioSource myAudioSource;
    public AudioClip openDoorSFX;
    public AudioClip closeDoorSFX;

    // Use this for initialization
    IEnumerator Start()
    {
        inspectable = GetComponent<Inspectable>();
        lockedItem = GetComponent<LockedItem>();
        animator = GetComponent<Animator>();
        myAudioSource = GetComponent<AudioSource>();
        yield return new WaitUntil(() => lockedItem.Unlocked);
        inspectable.AdditionalCommand = () => 
        {
            animator.SetBool("Closed", !animator.GetBool("Closed"));
            inspectable.InspectorUI.gameObject.SetActive(false);
            if(animator.GetBool("Closed"))              
                myAudioSource.PlayOneShot(closeDoorSFX);
            else
                myAudioSource.PlayOneShot(openDoorSFX);
        };
    }
}
