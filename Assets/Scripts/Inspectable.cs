﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inspectable : MonoBehaviour
{
    [HideInInspector]
    public cakeslice.Outline outline;

    public string Name;
    public string Description;
    public GameObject CloneObject;
    public List<GameObject> ObjectsToDisable = new List<GameObject>();
    public InspectorPanel InspectorUI;
    public Action AdditionalCommand;
    public bool HighlightOnly;
    public bool UpdateHighlight = true;
    [HideInInspector]
    public ObjectDetection ObjectDetector;
    GameObject thisObject;

    // Use this for initialization
    void Start()
    {
        while (!outline)
        {
            outline = GetComponent<cakeslice.Outline>();
            outline = GetComponentInChildren<cakeslice.Outline>();
        }
        outline.enabled = false;
        ObjectsToDisable.ForEach(obj => obj.SetActive(false));
        ObjectDetector = GameObject.FindGameObjectWithTag("Player").GetComponent<ObjectDetection>();
        thisObject = gameObject;
    }

    public void Highlight(bool on)
    {
        if (outline)
            outline.enabled = on;
        ObjectsToDisable.ForEach(obj => obj.SetActive(on));
    }

    // Update is called once per frame
    void Update()
    {
        if (UpdateHighlight)
            updateHighlight();
    }

    void updateHighlight()
    {
        if (ObjectDetector)
        {
            if (ObjectDetector.CurrentObject)
                Highlight(ObjectDetector.CurrentObject.Equals(thisObject));
            else
                Highlight(false);
        }
    }
}
