﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class Inventory : MonoBehaviour
{
    public Image Icon;
    public Text Name;
    public Text Description;

    public GameObject Player;
    public Transform ContentParent;
    public InventoryItem InventoryItemPrefab;

   // [HideInInspector]
    public List<InventoryItem> Items = new List<InventoryItem>();
   // [HideInInspector]
    public InventoryItem CurrentItem;

    // Use this for initialization
    void Start()
    {
        ClearInspection();
        if (Items.Count <= 0)
        {
            foreach (Transform child in ContentParent)
            {
                if (child.GetComponent<InventoryItem>())
                    Items.Add(child.GetComponent<InventoryItem>());
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
            gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        Player.GetComponent<FirstPersonController>().m_MouseLook.DisableLook();
        Player.GetComponent<FirstPersonController>().enabled = false;
        foreach (Transform child in ContentParent)
            child.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        Player.GetComponent<FirstPersonController>().enabled = true;
        Player.GetComponent<FirstPersonController>().m_MouseLook.EnableLook();
        Player.GetComponent<FirstPersonController>().m_MouseLook.InternalLockUpdate();
        foreach (Transform child in ContentParent)
            child.gameObject.SetActive(false);
    }

    public void ClearInspection()
    {
        Icon.color = Color.clear;
        Name.text = "";
        Description.text = "";
        CurrentItem = null;
    }

    public void UseItem()
    {
        CurrentItem.UseItem();
    }

    public void InspectItem(InventoryItem item)
    {
        Icon.sprite = item.Icon.sprite;
        Icon.color = Color.white;
        Name.text = item.Name;
        Description.text = item.Description;
        CurrentItem = item;
    }

    public void AddItem(InventoryItem inventoryItem, int quantity)
    {
        if (Items.Exists(i => i.UniqueID == inventoryItem.UniqueID))
            Items.Find(x => x.UniqueID == inventoryItem.UniqueID).StackCount += quantity;

        else
            for (int i = 0; i < quantity; i++)
            {
                Items.Add(inventoryItem);
                InventoryItem item = Instantiate(inventoryItem, ContentParent);
                item.Name = inventoryItem.Name;
                item.Description = inventoryItem.Description;
                item.Icon.sprite = inventoryItem.Icon.sprite;
            }
    }
}
