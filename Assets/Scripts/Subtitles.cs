﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Subtitles : MonoBehaviour
{
    public List<TextContent> Contents = new List<TextContent>();
    public float LastSubTimeStamp;
    int index;
    Text text;
    float deltaTime;
    // Use this for initialization
    void Start()
    {
        Contents.Sort((c1, c2) => c1.TimeStamp.CompareTo(c2.TimeStamp));
        index = 0;
        text = GetComponent<Text>();
        text.text = Contents[index].Content;
        deltaTime = 0; 
    }

    // Update is called once per frame
    void Update()
    {
        index = Mathf.Clamp(index, 0, Contents.Count - 1);
        deltaTime += Time.deltaTime;
        if (deltaTime > Contents[index].TimeStamp)
            text.text = Contents[index++].Content;
        if (deltaTime > LastSubTimeStamp)
            Destroy(gameObject.transform.parent.gameObject);
    }   
}
