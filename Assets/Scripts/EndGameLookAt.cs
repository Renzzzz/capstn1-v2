﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameLookAt : MonoBehaviour
{
    public void LookAt(GameObject target)
    {
        transform.LookAt(target.transform.position);
        StartCoroutine(endComic());
        //transform.position = Vector3.MoveTowards(transform.position,target.transform.position, 2);
        //transform.LookAt(target.transform.position);
        //Quaternion LookTo = Quaternion.LookRotation(target.transform.position - player.transform.position);
        //player.transform.rotation = Quaternion.Slerp(player.transform.rotation, LookTo, Time.deltaTime * speed);
    }
    IEnumerator endComic()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(2);
    }

}

