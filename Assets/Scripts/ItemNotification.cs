﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemNotification : MonoBehaviour
{
    public Text Name;
    public Text Description;
    public Image Icon;
    public AudioSource SoundSource;

    private void OnEnable()
    {
        SoundSource.Play();
    }
}
