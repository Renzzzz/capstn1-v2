﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubtitleManager : MonoBehaviour {

    public string displayedSubtitle;
    AudioClip subtitleclip;
    public Text textUI;
	
	void Start ()
    {
		
	}
	
	void Update ()
    {
        //textUI.text = displayedSubtitle;
        if (displayedSubtitle != "")
        {
            textUI.enabled = true;
            StartCoroutine(StartSubtitle());
        }
        else
        {
            textUI.enabled = false;
        }
	}

    IEnumerator StartSubtitle()
    {
        textUI.text = displayedSubtitle;
        yield return new WaitForSeconds(subtitleclip.length);
        displayedSubtitle = "";
        subtitleclip = null;
    }
    public void EnterSubtitle(string subtitle, AudioClip audio)
    {
        displayedSubtitle = subtitle;
        subtitleclip = audio;
    }
}
