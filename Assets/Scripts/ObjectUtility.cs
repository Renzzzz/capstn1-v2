﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectUtility : MonoBehaviour
{
    public void DestroyObject()
    {
        Destroy(gameObject);
    }
}
