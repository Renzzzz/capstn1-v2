﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Vault : MonoBehaviour
{
    public int Code;
    public List<Text> TextBlocks;
    public Button UnlockButton;
    public Button EnterButton;
    public Transform ButtonsParent;
    public RawImage StatusIndicator;
    public LockedItem LockedItemScript;
    public char EmptyCharacter;

    int currentBlock;
    Color initialStatusColor;

    AudioSource myAudioSource;
    public AudioClip phoneButtonPress;
    public AudioClip rightCombination;
    public AudioClip wrongCombination;

    // Use this for initialization
    void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
        currentBlock = 0;
        initialStatusColor = StatusIndicator.color;
        if (EnterButton)
            EnterButton.interactable = false;
        if (LockedItemScript)
            LockedItemScript.UnlockButton = null;
        TextBlocks.ForEach(block => block.text = EmptyCharacter.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            GetComponentInParent<Canvas>().gameObject.SetActive(false);
    }

    public void TypeText(string character)
    {
        myAudioSource.PlayOneShot(phoneButtonPress);
        if (currentBlock < TextBlocks.Count)
        {
            TextBlocks[currentBlock].text = character;
            currentBlock += (currentBlock > TextBlocks.Count) ? 0 : 1;
        }
        if (EnterButton)
            EnterButton.interactable = !TextBlocks.Exists(block => block.text == EmptyCharacter.ToString());
    }

    public void Backspace()
    {
        if (currentBlock >= 0)
        {
            TextBlocks[(currentBlock > 0 ? currentBlock - 1 : 0)].text = EmptyCharacter.ToString();
            currentBlock -= (currentBlock == 0) ? 0 : 1;
            if (EnterButton)
                EnterButton.interactable = !TextBlocks.Exists(block => block.text == EmptyCharacter.ToString());
        }
    }

    public void Enter()
    {
        string codeEntered = "";
        foreach (Text block in TextBlocks)
            codeEntered += block.text;
        if (Code == int.Parse(codeEntered))
        {
            myAudioSource.PlayOneShot(rightCombination);
            StatusIndicator.color = Color.green;
            if (LockedItemScript)
                LockedItemScript.Unlocked = true;
            foreach (Transform child in ButtonsParent)
                child.GetComponent<Button>().interactable = false;
            TextBlocks.ForEach(block => block.text = "-");
            if (UnlockButton)
                UnlockButton.interactable = true;
        }
        else
        {
            if (isActiveAndEnabled)
                StartCoroutine(wrongCodeColor());
            TextBlocks.ForEach(block => block.text = EmptyCharacter.ToString());
            currentBlock = 0;
            if (EnterButton)
                EnterButton.interactable = false;
        }
    }

    public void Reset()
    {
        StopAllCoroutines();
        TextBlocks.ForEach(block => block.text = EmptyCharacter.ToString());
        currentBlock = 0;
        if (EnterButton)
            EnterButton.interactable = false;
    }

    IEnumerator wrongCodeColor()
    {
        myAudioSource.PlayOneShot(wrongCombination);
        StatusIndicator.color = Color.red;
        yield return new WaitForSecondsRealtime(1);
        StatusIndicator.color = initialStatusColor;
    }
}
