﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class InspectorPanel : MonoBehaviour
{
    public Transform ObjectPlacement;
    public RawImage ObjectScreen;
    public Camera StageCamera;
    public Text NameText;
    public Text DescriptionText;
    public Toggle RotationToggle;
    public Text ContinueText;
    [HideInInspector]
    public GameObject CurrentObjectDisplayed;
    [HideInInspector]
    public GameObject Player;
    public GameObject NotifPanel;
    public bool RotateOn;
    public bool ZoomOn;

    Vector3 initialPosition;

    // Use this for initialization
    void Start()
    {
        RotateOn = false;
        initialPosition = StageCamera.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !NotifPanel.gameObject.activeInHierarchy)
        {
            gameObject.SetActive(false);
        }

        if (RotateOn)
        {
            if (Input.GetKeyUp(KeyCode.Mouse0))
                RotateOn = false;
        }
    }

    private void FixedUpdate()
    {
        if (RotateOn)
        {
            RotateObject();// CurrentObjectDisplayed.transform.Rotate(new Vector3(Input.GetAxis("Mouse Y"), -Input.GetAxis("Mouse X"), 0) * 10);
        }
        else
        {
            if (!RotationToggle.isOn)
                CurrentObjectDisplayed.transform.rotation = Quaternion.RotateTowards(CurrentObjectDisplayed.transform.rotation, Quaternion.identity, 5.5f);
        }

        if (ZoomOn)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
                StageCamera.transform.position = Vector3.MoveTowards(StageCamera.transform.position, Vector3.Lerp(initialPosition, CurrentObjectDisplayed.transform.position, 0.8f), 0.05f);

            else if (Input.GetAxis("Mouse ScrollWheel") < 0)
                StageCamera.transform.position = Vector3.MoveTowards(StageCamera.transform.position, initialPosition, 0.05f);
        }
    }

    void RotateObject()
    {
        float rotX = Input.GetAxis("Mouse X") * 10 * Mathf.Deg2Rad;
        float rotY = Input.GetAxis("Mouse Y") * 10 * Mathf.Deg2Rad;

        CurrentObjectDisplayed.transform.RotateAround(Vector3.up, -rotX);
        CurrentObjectDisplayed.transform.RotateAround(Vector3.right, rotY);
    }

    public void EnableRotation()
    {
        RotateOn = true;
    }

    public void EnableZoom()
    {
        ZoomOn = true;
    }

    public void DisableZoom()
    {
        ZoomOn = false;
    }

    private void OnEnable()
    {
        Player.GetComponent<FirstPersonController>().m_MouseLook.DisableLook();
        Player.GetComponent<FirstPersonController>().enabled = false;
    }

    private void OnDisable()
    {
        Player.GetComponent<FirstPersonController>().enabled = true;
        Player.GetComponent<FirstPersonController>().m_MouseLook.EnableLook();
        Player.GetComponent<FirstPersonController>().m_MouseLook.InternalLockUpdate();
        ContinueText.text = "[Space] To continue";
        Destroy(CurrentObjectDisplayed);
    }
}
