﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    public Inventory PlayerInvetory;
    public int FlashlightID;
    [HideInInspector]
    public Light LightSource;
    public AudioClip flashlightAudio;
    AudioSource myAudioSource;

    private void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
        LightSource = GetComponent<Light>();
        LightSource.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            LightSource.enabled = PlayerInvetory.Items.Exists(item => item.UniqueID == FlashlightID) ? !LightSource.isActiveAndEnabled : false;
            if(LightSource.enabled == true)
                myAudioSource.PlayOneShot(flashlightAudio);
        }

    }
}
