﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HardFollow : MonoBehaviour
{
    public Transform FollowObject;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (FollowObject.gameObject.activeInHierarchy)
            transform.position = FollowObject.position;
    }
}
