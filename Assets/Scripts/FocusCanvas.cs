﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusCanvas : MonoBehaviour
{
    public FocusInspect FocusObject;

    private void OnDisable()
    {
        FocusObject.gameObject.SetActive(true);
        FocusObject.BackToPlayer();
    }
}
