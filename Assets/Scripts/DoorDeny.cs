﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorDeny : MonoBehaviour
{
    public Animator DoorAnimator;
    public Cellphone CellPhone;
    private void OnTriggerEnter(Collider other)
    {
        if (CellPhone.Answered)
        {
            if (other.GetComponent<CharacterController>())
                DoorAnimator.SetBool("Closed", true);
        }
    }
}
