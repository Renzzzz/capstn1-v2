﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameStart : MonoBehaviour
{
    public float MoveRate;
    public FirstPersonController PlayerControls;
    public Camera PlayerCamera;
    public RawImage Overlay;
    public float ImageAlphaTransition;
    //FlickerLight flickerRef;

    // Use this for initialization
    IEnumerator Start()
    {
        while (PlayerCamera.gameObject.transform.localPosition.z > 0)
        {
            PlayerCamera.gameObject.transform.localPosition = Vector3.MoveTowards(PlayerCamera.gameObject.transform.localPosition, new Vector3(0, PlayerCamera.gameObject.transform.localPosition.y, 0), MoveRate);
            Overlay.color = new Color(Overlay.color.r, Overlay.color.g, Overlay.color.b, Mathf.MoveTowards(Overlay.color.a, 0, ImageAlphaTransition));
            yield return new WaitForFixedUpdate();
        }
        Overlay.gameObject.SetActive(false);
        PlayerControls.enabled = true;
    }   
}
