﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerThoughts : MonoBehaviour
{
    public AudioSource KillerAudio;
    AudioSource playerAudio;

    // Use this for initialization
    IEnumerator Start()
    {
        playerAudio = GetComponent<AudioSource>();
        yield return new WaitUntil(() => KillerAudio.isPlaying);
        yield return new WaitUntil(() => !KillerAudio.isPlaying);
        playerAudio.Play();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AudioEvent(AudioClip clip)
    {
        StartCoroutine(audioEvent(clip));
    }

    IEnumerator audioEvent(AudioClip clip)
    {
        playerAudio.clip = clip;
        yield return new WaitUntil(() => KillerAudio.isPlaying);
        yield return new WaitUntil(() => !KillerAudio.isPlaying);
        playerAudio.Play();
    }
}
