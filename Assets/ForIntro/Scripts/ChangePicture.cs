﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangePicture : MonoBehaviour
{
    public GameObject loadingScreen;
    public Slider slider;
    public Sprite[] mySprites;
    public AudioClip myAudioClip;
    public AudioClip lastAudioClip;
    public GameObject mainBG;
    AudioSource myAudioSource;
    Image myImageComponent;
    int x;

    void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
        myImageComponent = GetComponent<Image>();
        x = 0;
    }

	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            x++;
            myImageComponent.sprite = mySprites[x];
            myAudioSource.PlayOneShot(myAudioClip);

            if (x == mySprites.Length - 1)
            {
                //mainBG.SetActive(false);
                //myAudioSource.PlayOneShot(lastAudioClip);
                //myAudioSource.
                
            }
        }
        if(x >= mySprites.Length)
        {
            print("henlo world");
            SceneManager.LoadScene(1);
            if (loadingScreen)
            {
                //LoadLevel(1);
            }
            
        }
        
    }

    public void LoadLevel(int sceneIndex)
    {

        StartCoroutine(LoadAsync(sceneIndex));
    }

    IEnumerator LoadAsync(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        loadingScreen.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            slider.value = progress;

            yield return new WaitForFixedUpdate();
        }
    }
}
